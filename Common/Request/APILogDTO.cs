namespace Common.Request
{
    public class APILogDTO
    {
        public string Browser { get; set; }
        public string ErrorCode { get; set; }
        public string ExceptionId { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorDetails { get; set; }
        public int ExceptionBy { get; set; }
        public string APIUrl { get; set; }
        public string Params { get; set; }
        public string Queries { get; set; }
    }
}