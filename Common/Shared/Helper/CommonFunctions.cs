using System;
using System.Security.Cryptography;
using System.Text;

namespace Common.Helper
{
    public class CommonFunctions
    {
        public string EncryptPassword(string Password)
        {
            if (!string.IsNullOrEmpty(Password))
            {
                UnicodeEncoding UnicodeEncodingEncoding = null;
                string Encrypted = "";
                UnicodeEncodingEncoding = new UnicodeEncoding();
                byte[] hashBytes = UnicodeEncodingEncoding.GetBytes(Password);

                // compute SHA-1 hash.
                SHA1 SHA1Object = new SHA1CryptoServiceProvider();
                byte[] cryptPassword = SHA1Object.ComputeHash(hashBytes);
                Encrypted = Convert.ToBase64String(hashBytes);
                return Encrypted;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}