﻿namespace Common.Enum
{
    public class ResponseMessages
    {
        public static string UserNotAuthorize = "You are not authorized";
        public static string UnknownError = "Some unknown error occoured";
        public static string LoginSuccess = "Logged in successfully";
        public static string LoginFailed = "User email or password is incorrect";
        public static string RegistrationSuccessful = "Registration successfully !";
        public static string NotFound = "Not found";
        public static string BadRequest = "Something went wrong !";
    }
}
