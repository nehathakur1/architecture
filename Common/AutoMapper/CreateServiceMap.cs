using AutoMapper;
using Common.Request;
using Domain.Models;

namespace Common.AutoMapper
{
    public class CreateServiceMap : Profile
    {
        public CreateServiceMap()
        {
            CreateMap<RegisterModel, User>().ReverseMap();
        }
    }
}