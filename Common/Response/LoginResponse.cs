namespace Common.Response
{
    public class LoginResponse
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
    public class LoginCommonResponse
    {
        public LoginResponse Data { get; set; }
        public string Message { get; set; }
        public LoginCommonResponse(LoginResponse Data, string Message)
        {
            this.Data = Data;
            this.Message = Message;
        }
    }
}