namespace Common.Response
{
    public class SqlExceptionHandler
    {
        public string message;
        public string sql;
        public string parameters;

        public SqlExceptionHandler(string message, string sql, string parameters)
        {
            this.message = message;
            this.sql = sql;
            this.parameters = parameters;
        }
    }
}