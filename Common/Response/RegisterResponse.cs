namespace Common.Response
{
    public class RegisterResponse
    {
        public int UserId { get; set; }
    }
    public class RegisterCommonResponse
    {
        public RegisterResponse Data { get; set; }
        public string Message { get; set; }
        public RegisterCommonResponse(RegisterResponse Data, string Message)
        {
            this.Data = Data;
            this.Message = Message;
        }
    }
}