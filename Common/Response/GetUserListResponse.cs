using System.Collections.Generic;

namespace Common.Response
{
    public class GetUserListResponse : LoginResponse
    {
        public bool IsActive { get; set; }
    }
    public class GetCommonUserListResponse
    {
        public List<GetUserListResponse> Data { get; set; }
        public GetCommonUserListResponse(List<GetUserListResponse> Data)
        {
            this.Data = Data;
        }
    }
}