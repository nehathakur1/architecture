using Common.Request;
using Common.Response;

namespace Repository.Interfaces
{
    public interface IUserRepository
    {
        LoginCommonResponse Login(LoginDTO loginDTO);
        GetCommonUserListResponse GetUsers();
    }
}