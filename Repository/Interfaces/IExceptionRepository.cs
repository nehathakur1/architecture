using Domain.Models;

namespace Repository.Interfaces
{
    public interface IExceptionRepository
    {
       int SaveLogs(ExceptionLog exceptionLog);
    }
}