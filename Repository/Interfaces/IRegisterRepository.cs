using System.Threading.Tasks;
using Common.Response;
using Domain.Models;

namespace Repository.Interfaces
{
    public interface IRegisterRepository
    {
        Task<RegisterCommonResponse> Register(User model);
    }
}