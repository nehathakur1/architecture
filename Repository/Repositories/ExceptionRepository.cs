using Domain.Models;
using Repository.Interfaces;
using Repository.Interfaces.UOW;

namespace Repository.Repositories
{
    public class ExceptionRepository : GenericRepository<ExceptionLog>, IExceptionRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public ExceptionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        /// <summary>
        /// To save the logs in the database
        /// </summary>
        /// <paramname="model"></param>
        /// <returns></returns>
        public int SaveLogs(ExceptionLog model)
        {
            Insert(model);
            var errorId = _dbContext.SaveChanges();
            return errorId;
        }
    }
}