using System;
using System.Threading.Tasks;
using AutoMapper;
using Common.Enum;
using Common.Response;
using Domain.Models;
using Repository.Interfaces;
using Repository.Interfaces.UOW;

namespace Repository.Repositories
{
    public class RegisterRepository : GenericRepository<User>, IRegisterRepository
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper _mapper;
        public RegisterRepository(ApplicationDbContext dbContext, IMapper mapper) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<RegisterCommonResponse> Register(User user)
        {
            RegisterCommonResponse response = new RegisterCommonResponse(null, "");
            if (_dbContext != null)
            {
                try
                {
                    RegisterResponse registerResponse = new RegisterResponse();
                    await _dbContext.Users.AddAsync(user);
                    await _dbContext.SaveChangesAsync();
                    registerResponse.UserId = user.UserId;
                    response = new RegisterCommonResponse(registerResponse, ResponseMessages.RegistrationSuccessful);
                    return response;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return response;
        }
    }
}