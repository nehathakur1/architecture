using System;
using System.Linq;
using Common.Enum;
using Common.Request;
using Common.Response;
using Domain.Models;
using Repository.Interfaces;
using Repository.Interfaces.UOW;

namespace Repository.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public UserRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public LoginCommonResponse Login(LoginDTO loginDTO)
        {
            try
            {
                var loginCommonResponse = new LoginCommonResponse(null, "");
                var users = (from u in _dbContext.Users
                             where (u.Email == loginDTO.Email && u.Password == loginDTO.Password && u.IsActive == true && u.IsDeleted == false)
                             select new LoginResponse
                             {
                                 UserId = u.UserId,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Email = u.Email
                             }).FirstOrDefault();
                if (users != null)
                    loginCommonResponse = new LoginCommonResponse(users, ResponseMessages.LoginSuccess);
                else
                    loginCommonResponse = new LoginCommonResponse(users, ResponseMessages.LoginFailed);
                return loginCommonResponse;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public GetCommonUserListResponse GetUsers()
        {
            try
            {
                var getUserListResponse = (from u in _dbContext.Users
                                           where u.IsDeleted == false
                                           select new GetUserListResponse
                                           {
                                               UserId = u.UserId,
                                               FirstName = u.FirstName,
                                               LastName = u.LastName,
                                               Email = u.Email,
                                               IsActive = u.IsActive
                                           }).ToList();
                var response = new GetCommonUserListResponse(getUserListResponse);
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}