using Common.Request;
using Common.Response;

namespace Service.Services.Interfaces
{
    public interface IUserInterface
    {
        LoginCommonResponse Login(LoginDTO loginDTO);
        GetCommonUserListResponse GetUsers();
    }
}