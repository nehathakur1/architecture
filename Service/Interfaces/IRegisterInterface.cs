using System.Threading.Tasks;
using Common.Response;
using Domain.Models;

namespace Service.Interfaces
{
    public interface IRegisterInterface
    {
        Task<RegisterCommonResponse> Register(User model);
    }
}