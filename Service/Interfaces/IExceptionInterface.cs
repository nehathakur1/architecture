using Domain.Models;

namespace Service.Services.Interfaces
{
    public interface IExceptionInterface
    {
         int SaveLogs(ExceptionLog model);
    }
}