using Common.Request;
using Common.Response;
using Repository.Interfaces;
using Service.Services.Interfaces;

namespace Service.Services
{
    public class UserService : IUserInterface
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public LoginCommonResponse Login(LoginDTO loginDTO)
        {
            return _userRepository.Login(loginDTO);
        }
        public GetCommonUserListResponse GetUsers()
        {
            return _userRepository.GetUsers();
        }
    }
}