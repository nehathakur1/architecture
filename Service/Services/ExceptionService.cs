using Domain.Models;
using Repository.Interfaces;
using Service.Services.Interfaces;

namespace Service.Services
{
    public class ExceptionService: IExceptionInterface
    {
        private readonly IExceptionRepository _exceptionRepository;
        public ExceptionService(IExceptionRepository exceptionRepository)
        {
            _exceptionRepository = exceptionRepository;
        }

        public int SaveLogs(ExceptionLog model)
        {
            return _exceptionRepository.SaveLogs(model);
        }
    }
}