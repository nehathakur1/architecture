using System.Threading.Tasks;
using Common.Response;
using Domain.Models;
using Repository.Interfaces;
using Service.Interfaces;

namespace Services
{
    public class RegisterService: IRegisterInterface
    {
        private readonly IRegisterRepository _registerRepository;
        public RegisterService(IRegisterRepository registerRepository)
        {
            _registerRepository = registerRepository;
        }

        public async Task<RegisterCommonResponse> Register(User user)
        {
            return await _registerRepository.Register(user);
        }
    }
}