using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    public class ExceptionLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ExceptionLogId { get; set; }
        public string Browser { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string ErrorCode { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string ExceptionId { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string ErrorMessage { get; set; }
        public string ErrorDetails { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public int ExceptionBy { get; set; }
        public DateTime? ExceptionOn { get; set; } = DateTime.Now;
        public string APIUrl { get; set; }
        public string Params { get; set; }
        public string SQLException { get; set; }
        public string EntityException { get; set; }
        public string DotnetException { get; set; }
    }
}
