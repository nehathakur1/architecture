using Microsoft.EntityFrameworkCore;

namespace Domain.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<ExceptionLog> ExceptionLogs { get; set; }
        public DbSet<User> Users { get; set; }
    }
}