using Common.Helper;
using Common.Request;
using Common.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Services.Interfaces;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        private readonly IUserInterface _userService;
        public UserController(IUserInterface userService, IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _userService = userService;
        }

        [HttpPost("Login")]
        public IActionResult Login(LoginDTO loginDTO)
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            loginDTO.Password = commonFunctions.EncryptPassword(loginDTO.Password);
            LoginCommonResponse response = _userService.Login(loginDTO);
            if (response.Data != null)
                return Ok(response);
            return BadRequest(response);
        }

        [HttpGet]
        [Route("GetUsersList")]
        public GetCommonUserListResponse GetUsersList()
        {
            return _userService.GetUsers();
        }
    }
}