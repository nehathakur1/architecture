using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class BaseController : ControllerBase
    {
        private readonly IHttpContextAccessor _httpContext;
        public BaseController(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }
        [NonAction]
        protected int GetUserId()
        {
            var user = _httpContext.HttpContext.User?.FindFirstValue(ClaimTypes.NameIdentifier);
            int userId = int.TryParse(user, out var id) ? id : 0;
            return userId;
        }
    }
}