using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Common.Request;
using Service.Interfaces;
using Microsoft.AspNetCore.Http;
using Domain.Models;
using Common.Helper;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : BaseController
    {
        private readonly IRegisterInterface _registerService;
        private readonly IMapper _mapper;
        public RegisterController(IRegisterInterface registerService, IMapper mapper, IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _registerService = registerService;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                CommonFunctions commonFunctions = new CommonFunctions();
                var addUser = _mapper.Map<RegisterModel, User>(model);
                addUser.Password = commonFunctions.EncryptPassword(model.Password);
                addUser.IsActive = true;
                addUser.CreatedOn = DateTime.Now;
                addUser.CreatedBy = GetUserId();
                var response = await _registerService.Register(addUser);
                if (response.Data != null)
                    return Ok(response);
                else
                    return BadRequest(response);
            }
            return BadRequest();
        }
    }
}