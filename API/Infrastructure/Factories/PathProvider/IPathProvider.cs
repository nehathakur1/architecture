namespace API.Infrastructure.Factories.PathProvider
{
   public interface IPathProvider
    {
        string MapPath(string path);
    }
}