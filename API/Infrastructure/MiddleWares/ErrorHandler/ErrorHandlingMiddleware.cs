using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using API.Controllers;
using API.Infrastructure.Factories.LogFiles;
using API.Infrastructure.Factories.PathProvider;
using API.Infrastructure.MiddleWares.Exceptions;
using Common.Enum;
using Common.Request;
using Domain.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Repository.Interfaces;
using Shyjus.BrowserDetection;

namespace API.Infrastructure.MiddleWares.ErrorHandler
{
    public class ErrorHandlingMiddleware : BaseController
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly IPathProvider _pathProvider;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ErrorHandlingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory, IPathProvider pathProvider, IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<ErrorHandlingMiddleware>();
            _pathProvider = pathProvider;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task Invoke(HttpContext httpContext, IWebHostEnvironment env, IBrowserDetector detector, IExceptionRepository _exceptionService)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex, env, detector, _exceptionService);
            }
        }
        private Task HandleExceptionAsync(HttpContext httpContext, Exception exception, IWebHostEnvironment env, IBrowserDetector detector, IExceptionRepository _exceptionService)
        {
            APILogDTO apiLogDTO = new APILogDTO();
            HttpStatusCode statusCode;
            string log = string.Empty; int currentUserId = 0; 
            string parameters = string.Empty; string dbProfilerQuery = string.Empty; string spName = string.Empty;

            string host = httpContext.Request.Host.ToString();
            string prefix = host.Contains("http://localhost:5000") ? "" : "/api";

            var exceptionType = exception.GetType();
            long exceptionId = DateTime.Now.Ticks;
            string message = exception.Message;
            string browserName = detector.Browser.Name;
            var spNameTest = exception.Data["Procedure"];
            apiLogDTO.APIUrl = httpContext.Request.Scheme + "://" + httpContext.Request.Host + prefix + httpContext.Request.Path;

            if (exception.Data["sp"] != null)
            {
                spName = exception.Data["sp"].ToString() == null ? null : exception.Data["sp"].ToString();
                try
                {
                    parameters = Convert.ToString(JsonConvert.SerializeObject(exception.Data["param"]));
                    dbProfilerQuery = GetSQLProfilerQuery(spName, parameters);
                }
                catch { }
            }

            if (exception.Message.Contains("network-related"))
            {
                statusCode = HttpStatusCode.InternalServerError;
                log = JsonConvert.SerializeObject(new
                {
                    Id = exceptionId,
                    Browser = browserName,
                    Status = statusCode,
                    Error = message,
                    APIUrl = apiLogDTO.APIUrl,
                    Params = parameters
                });
            }
            else
            {
                if (exceptionType == typeof(UnauthorizedAccessException))
                {
                    statusCode = HttpStatusCode.Forbidden;
                    log = ResponseMessages.UserNotAuthorize;
                }
                else if (exceptionType == typeof(BadRequestException))
                {
                    statusCode = HttpStatusCode.BadRequest;
                    log = JsonConvert.SerializeObject(new
                    {
                        Id = exceptionId,
                        Browser = browserName,
                        Status = statusCode,
                        Error = message,
                        APIUrl = apiLogDTO.APIUrl,
                        Params = parameters
                    });
                }
                else if (exceptionType == typeof(NotFoundException))
                {
                    statusCode = HttpStatusCode.NotFound;
                    log = JsonConvert.SerializeObject(new
                    {
                        Id = exceptionId,
                        Browser = browserName,
                        Status = statusCode,
                        Error = message,
                        APIUrl = apiLogDTO.APIUrl,
                        Params = parameters
                    });
                }
                else
                {
                    statusCode = HttpStatusCode.BadRequest;
                    log = ResponseMessages.UnknownError;
                }
            }

            var errorId = _exceptionService.SaveLogs(new ExceptionLog()
            {
                Browser = browserName,
                ExceptionBy = currentUserId,
                ExceptionId = exceptionId.ToString(),
                ErrorMessage = message,
                ErrorCode = statusCode.ToString(),
                ErrorDetails = exception.StackTrace.ToString(),
                APIUrl = apiLogDTO.APIUrl,
                Params = parameters,
                SQLException = dbProfilerQuery,
                EntityException = exception.StackTrace.ToString()
            });
            if (errorId == 0)
            {
                CreateLogFiles.CreateFileIfNotExist(_pathProvider);
                CreateLogFiles.Log(_pathProvider, log, exception.StackTrace.ToString());
                _logger.LogError(exception, exception.Message);
            }

            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)statusCode;
            return httpContext.Response.WriteAsync(log);
        }

        private static string GetSQLProfilerQuery(string storedProcedure, string parameters)
        {
            string profilerQuery = string.Empty;
            if (!String.IsNullOrEmpty(storedProcedure))
            {
                string databaseQuery = string.Empty;
                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(parameters);
                foreach (var kv in dict)
                {
                    databaseQuery += "@" + kv.Key + "='" + kv.Value + "',";
                }
                databaseQuery = databaseQuery.TrimEnd(',');
                profilerQuery = "exec " + storedProcedure + " " + databaseQuery;
                return profilerQuery;
            }
            else
                return profilerQuery;
        }
    }
}