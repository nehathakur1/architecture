using API.Infrastructure.MiddleWares.CustomMiddleware;
using Microsoft.AspNetCore.Builder;

namespace API.Extensions
{
    public static class CustomMiddlewareExtension
    {
        public static IApplicationBuilder UseCustomMiddleware(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder.UseMiddleware<CustomMiddleware>();
        }
    }
}