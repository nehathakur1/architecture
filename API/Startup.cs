using API.Extensions;
using AutoMapper;
using Common.AutoMapper;
using Common.Response;
using Domain.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Excepticon.AspNetCore;
using Excepticon.Extensions;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Repository.Interfaces;
using Service.Services;
using Repository.Repositories;
using API.Infrastructure.Factories.PathProvider;
using Service.Interfaces;
using Services;
using Microsoft.AspNetCore.Http;
using API.Infrastructure.MiddleWares.ErrorHandler;
using API.Infrastructure.MiddleWares.CustomMiddleware;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        private readonly string corsPolicy = "corsPolicy";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                options.EnableSensitiveDataLogging();
            });
            services.AddControllers();
            services.AddHttpClient();

            var appSettingsSection = Configuration.GetSection("AppSettings");
            var appSettings = appSettingsSection.Get<AppSettings>();

            // Set all appsetting configuration in static object 
            AppSettingConfigurations.AppSettings = appSettings;

            services.SwaggerImplementations(Configuration);
            services.AddExcepticon();
            services.AddBrowserDetection();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IPathProvider, PathProvider>();
            services.AddTransient<IExceptionInterface, ExceptionService>();
            services.AddTransient<IExceptionRepository, ExceptionRepository>();
            services.AddTransient<IUserInterface, UserService>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddScoped<IRegisterRepository, RegisterRepository>();
            services.AddScoped<IRegisterInterface, RegisterService>();
            services.AddCors(o =>
            {
                o.AddPolicy(name: corsPolicy,
                              builder =>
                              {
                                  builder.AllowAnyOrigin()
                                      .AllowAnyMethod()
                                      .AllowAnyHeader()
                                      .WithExposedHeaders("AttachmentId", "Upload-Offset", "Location", "Upload-Length", "Tus-Version", "Tus-Resumable", "Tus-Max-Size", "Tus-Extension", "Upload-Metadata");
                              });
            });
            MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new CreateServiceMap());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCustomMiddleware();
            app.UseSwagger();
            app.UseStaticFiles();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", "Demo");
            });
            //app.UseLogger();
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseExcepticon();
            app.UseRouting();
            app.UseCors(corsPolicy);
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
